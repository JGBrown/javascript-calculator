import React from 'react';
import './MainContainer.css'

class MainContainer extends React.Component {
  render() {
    return (
      <div className="MainBox">
        <div id="display" className="display">0</div>
        <div className="numPad">
        <div id="one" className="calcButton">1</div>
        <div id="two" className="calcButton">2</div>
        <div id="three" className="calcButton">3</div>
        <div id="four" className="calcButton">4</div>
        <div id="five" className="calcButton">5</div>
        <div id="six" className="calcButton">6</div>
        <div id="seven" className="calcButton">7</div>
        <div id="eight" className="calcButton">8</div>
        <div id="nine" className="calcButton">9</div>
        <div id="zero" className="calcButton">0</div>
        <div id="decimal" className="calcButton">.</div>
        <div id="plus" className="calcButton">+</div>
        <div id="minus" className="calcButton">-</div>
        <div id="times" className="calcButton">X</div>
        <div id="divide" className="calcButton">%</div>
        <div id="clear" className="calcButton">C</div>
        <div id="equals" className="calcButton">=</div>
        </div>
      </div>
    )
  }
}

export default MainContainer;
